from itertools import cycle
from string import ascii_lowercase
from random import choices
import timeit

def palindrom(N: int, K: int) -> str:
    """Creates a palindom of length N using K random characters"""
    chosen = cycle(choices(ascii_lowercase, k=K))
    seq = ""
    seq_len = int(N/2)
    for _ in range(seq_len):
        seq += next(chosen)
    return seq + next(chosen) + seq[::-1]

def palindromV2(N: int, K: int) -> str:
    """Version using string manipulation"""
    chosen = "".join(choices(ascii_lowercase, k=K))
    even = N % 2 == 0
    mid = int(N / 2)
    repeat, remainder = (int(mid/K), mid % K) if K <= mid else (0, mid%K)
    part = chosen * repeat + chosen[:remainder]
    if even:
        return part + part[::-1]
    else:
        return part + chosen[-1] + part[::-1]

def validate_palindrom(palindrom: str):
    """Validate Palindrom"""
    pal_len = len(palindrom)
    even = pal_len%2 == 0
    mid = int(pal_len / 2)
    if even:
        return palindrom[:mid] == palindrom[mid:][::-1]
    else:
        return palindrom[:mid] == palindrom[mid+1:][::-1]


starttime = timeit.default_timer()
print("The start time is :",starttime)
pal3 = palindromV2(110000000, 6)
print("The time difference is :", timeit.default_timer() - starttime)

starttime = timeit.default_timer()
print("The start time is :",starttime)
pal3 = palindrom(110, 6)
print("The time difference is :", timeit.default_timer() - starttime)

pal3 = palindromV2(11, 6)
pal1 = palindrom(4,2)
pal2 = palindromV2(5, 3)
print(pal1,pal2,pal3)
print("Palindrom is valid:", validate_palindrom(pal1))
print("Palindrom is valid:", validate_palindrom(pal2))
print("Palindrom is valid:", validate_palindrom(pal3))
print("Palindrom is valid:", validate_palindrom("abba"))
print("Palindrom is valid:", validate_palindrom("abcfcba"))
print("Palindrom is valid:", validate_palindrom("aaaaaaa"))
print("Palindrom is valid:", validate_palindrom("aaaaaaba"))